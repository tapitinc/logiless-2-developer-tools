#!/bin/sh

# Variables
project=$(cd $(dirname $0) && pwd)
project=`echo ${project/\/usr\/local\/rms\/evt\//}`
project=`echo ${project/\/shell/}`

from=/mnt/workspace/${project}/
to=/usr/local/rms/evt/${project}/

echo "FROM : ${from}"
echo "TO   : ${to}"

# Commnads
#rsync -avzln --delete --exclude /bootstrap.php.cache --exclude /cache --exclude /logs --exclude /pear --exclude /session ${from}app/ ${to}app/
#rsync -avzln --delete --exclude '*.git*' --exclude 'templates.js'             ${from}src/ ${to}src/
#rsync -avzln --delete --exclude /bundles --exclude /resources --exclude /dist ${from}web/ ${to}web/

echo "-------------------------------------------"
echo "Are you sure rsync? [Press Y(Yes) or N(No)]"
echo -n " => "
read ans
echo "-------------------------------------------"
case ${ans} in
  Y)
    continue ;;
  *)
    echo "Canceled."
    exit 0;;
esac

rsync -avzl --delete --exclude /bootstrap.php.cache --exclude /cache --exclude /logs --exclude /pear --exclude /session ${from}app/ ${to}app/
rsync -avzl --delete --exclude '*.git*' --exclude 'templates.js'             ${from}src/ ${to}src/
#rsync -avzl --delete --exclude /bundles --exclude /resources --exclude /dist ${from}web/ ${to}web/

