#!/bin/sh

# Variables
project=$(cd $(dirname $0) && pwd)
project=`echo ${project/\/usr\/local\/rms\/evt\//}`
project=`echo ${project/\/shell/}`

from=/usr/local/rms/evt/${project}/
to=/mnt/workspace/${project}/

echo "FROM : ${from}"
echo "TO   : ${to}"

# Commnads
rsync -avzln --delete --exclude /bootstrap.php.cache --exclude /cache --exclude /logs --exclude /pear --exclude /session ${from}app/    ${to}app/
rsync -avzln --delete --exclude '*.git*'                                      ${from}src/    ${to}src/
rsync -avzln --delete --exclude /bundles --exclude /resources --exclude /dist ${from}web/    ${to}web/
#rsync -avzln --delete                                                         ${from}vendor/ ${to}vendor/

echo "-------------------------------------------"
echo "Are you sure rsync? [Press y(Yes) or N(No)]"
echo -n " => "
read ans
echo "-------------------------------------------"
case ${ans} in
  y)
    continue ;;
  *)
    echo "Canceled."
    exit 0;;
esac

rsync -avzl --delete --exclude /bootstrap.php.cache --exclude /cache --exclude /logs --exclude /pear --exclude /session ${from}app/    ${to}app/
rsync -avzl --delete --exclude '*.git*'                                      ${from}src/    ${to}src/
rsync -avzl --delete --exclude /bundles --exclude /resources --exclude /dist ${from}web/    ${to}web/
#rsync -avzl --delete                                                         ${from}vendor/ ${to}vendor/

cp    ${from}app/cache/dev/appDevDebugProjectContainer.xml ${to}app/cache/dev/.
cp    ${from}app/cache/dev/appDevUrlGenerator.php          ${to}app/cache/dev/. 
cp -r ${from}app/cache/dev/translations                    ${to}app/cache/dev/. 

